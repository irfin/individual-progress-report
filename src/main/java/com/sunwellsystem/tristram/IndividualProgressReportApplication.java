package com.sunwellsystem.tristram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndividualProgressReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndividualProgressReportApplication.class, args);
	}

}
